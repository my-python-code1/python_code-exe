#problem de timeseries: phenomene qui evolue avec le temps
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
bitcoin = pd.read_csv('D:/Telechargements/BTC-EUR.csv')
bitcoin = pd.read_csv('D:/Telechargements/BTC-EUR.csv', index_col='Date', parse_dates=True)

#moving average: cal une moyenne sur une fenetre de valeur 
# on définit une fct qui effectue le roulement
#et à l'interieur on définit une taille

#bitcoin.loc['2019', 'Close'].rolling(window=7).mean().plot()

plt.figure(figsize=(8, 6))
bitcoin.loc['2019-09', 'Close'].plot()
bitcoin.loc['2019-09', 'Close'].rolling(window=7).mean().plot(label='movingaverage',lw=3, ls=':', alpha=0.8)
bitcoin.loc['2019-09', 'Close'].rolling(window=7, center=True).mean().plot(label='movingaverage',lw=3, ls=':', alpha=0.8)
#center=true redecalle les valeur 
bitcoin.loc['2019-09', 'Close'].ewm(alpha=0.6).mean().plot(label='movingaverage',lw=3, ls=':', alpha=0.8)
plt.legend()
plt.show()


